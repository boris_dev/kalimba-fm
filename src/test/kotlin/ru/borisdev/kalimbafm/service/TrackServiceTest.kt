package ru.borisdev.kalimbafm.service

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.util.Assert
import ru.borisdev.kalimbafm.TrackService
import ru.borisdev.kalimbafm.domain.*
import ru.borisdev.kalimbafm.repository.TrackRepository

internal class TrackServiceTest{


    @Test
    fun transpositionByMidiTest() {
        val result: List<TrackDto> = TrackService(TrackRepository()).transpositionByMidi(
            TrackRepository().findById(907),
            34,
            listOf(NoteSign.H_5,NoteSign.F_SHARP_5,NoteSign.D_5,NoteSign.A_4,NoteSign.E_4,NoteSign.H_3,NoteSign.A_3,NoteSign.D_4,NoteSign.F_SHARP_4,NoteSign.H_4,NoteSign.E_5,NoteSign.A_5),
            4)
        assertEquals(result.get(0).tonality, Tonality.F)
        assertEquals(result.get(0).tonalityDiff, 8)
        assertEquals(result.get(0).differenceInNotes, 2)
        assertEquals(result.get(0).uniqNotes, setOf(NoteSign.H_3, NoteSign.F_SHARP_5, NoteSign.D_5, NoteSign.H_4, NoteSign.A_3, NoteSign.F_SHARP_4, NoteSign.D_4, NoteSign.A_4,NoteSign.E_4, NoteSign.E_5))
    }
}