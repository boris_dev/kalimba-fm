package ru.borisdev.kalimbafm.service

import org.junit.jupiter.api.Test
import ru.borisdev.kalimbafm.domain.*


internal class TabsServiceTest {
    private val tabsService: TabsService = TabsService()

    @Test
    fun `Рисуем ноты для калимбы си-минор`() {
        println(
            tabsService.tabs(
                kalimbaPentatonic(),
                KTabsType.SIMPLE_TABS,
                kalimbaBm()
            )
        )
    }

    private fun kalimbaBm(): List<NoteSign> {
        return listOf(
            NoteSign.A_5,
            NoteSign.E_5,
            NoteSign.H_4,
            NoteSign.F_SHARP_4,
            NoteSign.D_4,
            NoteSign.A_3,
            NoteSign.H_3,
            NoteSign.E_4,
            NoteSign.A_4,
            NoteSign.D_5,
            NoteSign.F_SHARP_5,
            NoteSign.H_5
        )
    }

    private fun kalimbaPentatonic(): VbTrack {
        return VbTrack(
            1,
            "Калимба пентатоника",
            "Теория музыки",
            null,
            Tonality.Hm,
            listOf(
                KNote(NoteSign.D_4, 0, 0),
                KNote(NoteSign.D_4, 0, 0),
                KNote(NoteSign.F_SHARP_4, 0, 0),
                KNote(NoteSign.F_SHARP_4, 0, 0),
                KNote(NoteSign.H_4, 0, 0),
                KNote(NoteSign.H_4, 0, 0),
                KNote(NoteSign.E_5, 0, 0),
                KNote(NoteSign.E_5, 0, 0),
                KNote(NoteSign.A_5, 0, 0),
                KNote(NoteSign.A_5, 0, 0),
            ), setOf()
        )

    }
}