package ru.borisdev.kalimbafm

import com.wrapper.spotify.SpotifyApi
import com.wrapper.spotify.model_objects.IPlaylistItem
import com.wrapper.spotify.model_objects.specification.Paging
import com.wrapper.spotify.model_objects.specification.Playlist
import com.wrapper.spotify.model_objects.specification.PlaylistTrack
import com.wrapper.spotify.model_objects.specification.Track
import org.springframework.stereotype.Service
import ru.borisdev.kalimbafm.domain.SpotifyTrack


@Service
object SpotifyClient {

    private lateinit var spotifyApi: SpotifyApi

    init {
//        spotifyApi = SpotifyApi.Builder()
//                .setClientId("cf75a2b9e1bc4084aaf4122d49722521")
//                .setClientSecret("3272ee74ad594bb38bd781bc936ab9cc")
//                .build()
//        val build = spotifyApi.clientCredentials()
//                .build()
//        try {
//            val clientCredentials = build.execute()
//            spotifyApi.accessToken = clientCredentials.accessToken
//        } catch (e: Exception) {
//            val clientCredentials = build.execute()
//            spotifyApi.accessToken = clientCredentials.accessToken
//        }

    }

    fun playlist(playlistId: String): List<SpotifyTrack> {
        val playlist = spotifyApi.getPlaylist(playlistId).build().execute()
        return allTracks(playlist)
                .map { createSpotifyTrack(it.track) }
    }

    private fun allTracks(playlist: Playlist): List<PlaylistTrack> {
        var list = mutableListOf<PlaylistTrack>()
        val items = playlist.tracks.items
        list.addAll(items)
        playlist.tracks.next
        list.addAll(playlist.tracks.items)
        return list
    }

    fun searchTracks(q: String): Array<Track> {
        return spotifyApi
                .searchTracks(q)
                .limit(10)
                .build()
                .execute().items
    }

    private fun createSpotifyTrack(track: IPlaylistItem): SpotifyTrack {
        val features = spotifyApi.getAudioFeaturesForTrack(track.id).build().execute()
        return SpotifyTrack(track.name, features.key.toString(), features.mode, features.tempo)
    }


}

