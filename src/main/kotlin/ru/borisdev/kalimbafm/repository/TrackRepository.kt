package ru.borisdev.kalimbafm.repository

import org.springframework.stereotype.Repository
import ru.borisdev.kalimbafm.domain.VbTrack
import ru.borisdev.vocaberry.db.SongDb

@Repository
class TrackRepository {
    private final val trackMap: Map<Long, VbTrack>

    init {
        val songDb = SongDb(
            "jdbc:mysql://smalugq4.beget.tech/smalugq4_vbsongs?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
            "smalugq4_vbsongs",
            """vbsongs$4"""
        )
        trackMap = songDb.findAllMidiReferences()
            .associateBy { it.id }
    }

    fun findAll(): List<VbTrack> {
        return trackMap.map { it.value }
    }

    fun findAllWithUniqNotes(): List<VbTrack> {
        return findAll().filter { e -> e.uniqNotes.isNotEmpty() }
    }

    fun findById(id: Long): VbTrack {
        return findAll().find { e -> e.id == id } ?: throw NoSuchElementException("not find track by id = ${id}")
    }
}
