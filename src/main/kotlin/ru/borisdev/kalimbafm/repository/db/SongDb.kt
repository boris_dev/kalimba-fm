package ru.borisdev.vocaberry.db

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import ru.borisdev.kalimbafm.domain.KNote
import ru.borisdev.kalimbafm.domain.NoteSign
import ru.borisdev.kalimbafm.domain.Tonality
import ru.borisdev.kalimbafm.domain.VbTrack
import java.sql.Connection
import java.sql.DriverManager

class SongDb(val url: String, val user: String, val pass: String) {
    private val mapper = ObjectMapper().registerKotlinModule()

    fun createBegetConnection(): Connection {
        try {
            // create a java mysql database connection
            val myDriver = "com.mysql.cj.jdbc.Driver"
            Class.forName(myDriver)
            return DriverManager.getConnection(url, user, pass)
        } catch (e: Exception) {
            System.err.println("Got an exception! ")
            System.err.println(e.message)
            throw e
        }
    }

    fun findAllMidiReferences(): List<VbTrack> {
        val connection = createBegetConnection()
        val list = mutableListOf<VbTrack>()
        try {
            val prepareStatement =
                connection.prepareStatement("SELECT s.id, s.name, a.band, s.tonality, s.lang, s.popularity, s.tempo,s .uniqNotes, s.notes FROM smalugq4_vbsongs.songs s, smalugq4_vbsongs.artists a  WHERE a.id=s.artistId and notes is not null")
            val result = prepareStatement.executeQuery()
            while (result.next()) {
                list.add(
                    VbTrack(
                        result.getLong("id"),
                        result.getString("name"),
                        result.getString("band"),
                        null,
                        Tonality.fromTitle(result.getString("tonality") ?: Tonality.NONE.tonalityName) ,
                        mapper.readValue(result.getString("notes"), object : TypeReference<List<KNote>>() {}),
                        mapper.readValue(result.getString("uniqNotes"), object : TypeReference<Set<NoteSign>>() {})
                    )
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            connection.close()
        }
        return list
    }
}
