package ru.borisdev.vocaberry.db

import ru.borisdev.kalimbafm.domain.KNote

data class MidiReference(
    val songId: Long,
    val ref: String,
    var localRef: String? = null,
    var uniqNotes: Set<String>? = null,
    var notes: List<KNote>? = null
)
