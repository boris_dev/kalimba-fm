package ru.borisdev.kalimbafm.rest

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import ru.borisdev.kalimbafm.TrackService
import ru.borisdev.kalimbafm.domain.Tonality

@RestController
class KalimbaKeyController(val trackService: TrackService) {

    @GetMapping("/tracks")
    fun tracks(tonality: Tonality): String {
        return trackService.topTracksWithKey(tonality).toString()
    }

    @GetMapping("/all")
    fun all(): String {
        return trackService.all()
    }

}