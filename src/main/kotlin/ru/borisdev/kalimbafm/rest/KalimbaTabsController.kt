package ru.borisdev.kalimbafm.rest

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.borisdev.kalimbafm.TrackService
import ru.borisdev.kalimbafm.domain.KTabsType
import ru.borisdev.kalimbafm.domain.NoteSign
import ru.borisdev.kalimbafm.domain.TrackDto
import ru.borisdev.kalimbafm.service.TabsService

@RestController
class KalimbaTabsController(val trackService: TrackService, val tabsService: TabsService) {

    val logger = LoggerFactory.getLogger(KalimbaTabsController::class.java)

    @GetMapping("/songs")
    fun songs(
        @RequestParam instrumentNotes: List<String>,
        @RequestParam diffThreshold: Int
    ): List<TrackDto> {
        logger.debug("[KalimbaTabsController] songs(${instrumentNotes}, ${diffThreshold})")
        return trackService.tracksByNotes(instrumentNotes.toNoteSign(), diffThreshold)
    }

    @GetMapping("/songs/{trackId}/tabs")
    fun tabs(
        @PathVariable trackId: Long,
        @RequestParam type: KTabsType,
        @RequestParam instrumentNotes: List<String>,
        @RequestParam tonalityDiff: Int
    ): String {
        logger.debug("[KalimbaTabsController] tabs(${trackId}, ${type}, ${instrumentNotes}, ${tonalityDiff})")
        val track = trackService.findById(trackId)
        logger.debug("[KalimbaTabsController] track find by id = ${track}")

        val transposedTrack = trackService.transpose(track, tonalityDiff)
        logger.debug("[KalimbaTabsController] new transposed track = ${transposedTrack}")

        return tabsService.tabs(transposedTrack, type, instrumentNotes.toNoteSign())
    }
}

fun List<String>.toNoteSign(): List<NoteSign> {
    return this.map { NoteSign.fromFullName(it) }
}