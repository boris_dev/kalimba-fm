package ru.borisdev.kalimbafm.service

import lombok.extern.slf4j.Slf4j
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import ru.borisdev.kalimbafm.domain.KNote
import ru.borisdev.kalimbafm.domain.KTabsType
import ru.borisdev.kalimbafm.domain.NoteSign
import ru.borisdev.kalimbafm.domain.VbTrack

@Service
@Slf4j
class TabsService {
    val logger = LoggerFactory.getLogger(TabsService::class.java)

    fun tabs(track: VbTrack, type: KTabsType, instrumentNotes: List<NoteSign>): String {
        return when (type) {
            KTabsType.SIMPLE_TABS -> printSimpleTabs(track.notes, instrumentNotes)
            KTabsType.LETTER -> printLetterTabs(track.notes)
            else -> throw NotImplementedError("$type is not supported yet")
        }
    }

    private fun printLetterTabs(notes: List<KNote>): String {
        logger.debug("[TabsService] printLetterTabs notes = ${notes}")
        var result = ""
        for (note in notes) {
            var sound = note.startTick + note.durationTicks * 2
            var kNote = findEndSound(notes, sound)
            result += note.note.noteName + note.note.octave
            if (kNote == null) {
                result += "\t"
            }
        }
        logger.debug("[TabsService] pause line = ${result}")
        return result
    }

    private fun findEndSound(notes: List<KNote>, sound: Long): KNote? {
        return notes.find { e -> e.startTick == sound }
    }

    private fun printSimpleTabs(notes: List<KNote>, instrumentNotes: List<NoteSign>): String {
        val tabs = notes
            .map { note -> printLineWithNote(note, instrumentNotes) + "\n" }
            .reduce { tab, line -> tab + line }
        return instrumentNotes.asString() + "\n" + tabs
    }

    private fun printLineWithNote(note: KNote, instrumentNotes: List<NoteSign>): String {
        return instrumentNotes
            .map { instrumentNote -> printNoteOrEmpty(note, instrumentNote) }
            .reduce { tab, line -> tab + line }

    }

    private fun printNoteOrEmpty(note: KNote, instrumentNote: NoteSign): String {
        return if (note.note == instrumentNote) {
            "x "
        } else {
            "- "
        }
    }
}

private fun List<NoteSign>.asString(): String {
    return this
        .map { note -> note.fullName() }
        .reduce { acc, note -> acc + " " + note }
}
