package ru.borisdev.kalimbafm

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import ru.borisdev.kalimbafm.domain.*
import ru.borisdev.kalimbafm.repository.TrackRepository


@Service
class TrackService(val trackRepository: TrackRepository) {

    val logger = LoggerFactory.getLogger(TrackService::class.java)

    val tracks: MutableList<SpotifyTrack> = mutableListOf()

    init {
//        tracks.addAll(spotifyClient.playlist("2LOxEzC4KmoWJ9NhW0kz5M"))
    }

    fun topTracksWithKey(tonality: Tonality): List<SpotifyTrack> {
        return tracks.filter { it.tonality() == tonality }
    }

    fun all(): String {
        return tracks.toString()
    }

    fun tracksByNotes(notes: List<NoteSign>, diffThreshold: Int): List<TrackDto> {
        val tracks = trackRepository.findAllWithUniqNotes()
        val result = mutableListOf<TrackDto>()

        for (track in tracks) {
            val upperTonalityLimit = fromHighMidi(notes.toHashSet()).midi - fromLowMidi(track.uniqNotes).midi
            val lowTonalityLimit = fromLowMidi(notes.toHashSet()).midi - fromHighMidi(track.uniqNotes).midi
            logger.debug("[TrackService] tracksByNotes() upperTonalityLimit = $upperTonalityLimit, lowTonalityLimit = $lowTonalityLimit")

            result.addAll(transpositionByMidi(track, upperTonalityLimit, notes, diffThreshold))
            result.addAll(transpositionByMidi(track, lowTonalityLimit, notes, diffThreshold))
        }
        logger.debug("[TrackService] tracksByNotes() found track: ${result.size}")
        result.sortBy { e -> e.differenceInNotes }

        return result
    }

    fun transpositionByMidi(
        track: VbTrack,
        tonalityLimit: Int,
        notes: List<NoteSign>,
        diffThreshold: Int
    ): List<TrackDto> {
        val shiftedFiltered = shiftNoteByMidi(track.uniqNotes, tonalityLimit).filter {
                e -> (e.values.toList()[0].size + diffThreshold >= notes.size) && (e.values.toList()[0].size - diffThreshold <= notes.size)
        }
        val result = mutableListOf<TrackDto>()
        for (listsSong in shiftedFiltered) {
            if (notes.isNotEmpty() &&
                notes.containsAll(listsSong.entries.elementAt(0).value)
            ) {
                logger.debug("[TrackService] transpositionByMidi() found track = ${track} by notes = ${notes}")
                val tonalityDiff = getTonalityDiff(notes, track)
                result.add(
                    TrackDto(
                        track.id,
                        track.name,
                        track.artist,
                        track.spotifyLink,
                        track.tonality.transpose(tonalityDiff),
                        listsSong.entries.elementAt(0).key,
                        Math.abs(notes.size - track.uniqNotes.size),
                        listsSong.entries.elementAt(0).value.toSet()
                    )
                )
            }
        }
        return result
    }

    private fun getTonalityDiff(
        notes: List<NoteSign>,
        track: VbTrack
    ): Int {
        val midiTrack = fromHighMidi(track.uniqNotes).midi
        val midiKalimba = fromHighMidi(notes.toSet()).midi
        return if (midiKalimba > midiTrack) midiTrack - midiKalimba else if (midiKalimba < midiTrack) midiTrack - midiKalimba else 0
    }

    fun fromLowMidi(notes: Set<NoteSign>): NoteSign {
        return notes.minOf { e -> e }
    }

    fun fromHighMidi(notes: Set<NoteSign>): NoteSign {
        return notes.maxOf { e -> e }
    }

    fun shiftNoteByMidi(notes: Set<NoteSign>, sequence: Int): List<Map<Int, List<NoteSign>>> {
        var noteResult = notes.toMutableList()
        val noteResultList = mutableListOf<Map<Int, List<NoteSign>>>()
        var i = 0
        while (kotlin.math.abs(i) <= kotlin.math.abs(sequence)) {
            for (note in noteResult) {
                noteResult[noteResult.indexOf(note)] = note.transpose(i)
            }
            noteResultList.add(mapOf(i to noteResult))
            noteResult = notes.toMutableList()
            if (sequence >= 0) {
                i++
            } else {
                i--
            }
        }
        return noteResultList
    }


    fun findById(trackId: Long): VbTrack {
        return trackRepository.findById(trackId)
    }

    fun transpose(track: VbTrack, tonalityDiff: Int): VbTrack {
        logger.debug("[TrackService] transpose(${track}, ${tonalityDiff})")
        val notesList = mutableListOf<KNote>()
        val newUniqNotes = mutableSetOf<NoteSign>()
        for (note in track.notes) {
            val noteSign = note.note.transpose(tonalityDiff)
            notesList.add(KNote(noteSign, note.startTick, note.durationTicks))
            newUniqNotes.add(noteSign)
        }
        logger.debug("[TrackService] transpose() new leaf of uniq notes = $newUniqNotes")
        return VbTrack(track.id, track.name, track.artist, track.spotifyLink, track.tonality, notesList, newUniqNotes)
    }


}

