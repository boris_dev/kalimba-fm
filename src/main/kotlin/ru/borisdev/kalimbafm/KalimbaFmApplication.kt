package ru.borisdev.kalimbafm

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KalimbaFmApplication

/**
 * 1. Индексация контента по плейлистам
2. Запрос песен с ссылками на spotify по тональности
 */
fun main(args: Array<String>) {
    runApplication<KalimbaFmApplication>(*args)
}
