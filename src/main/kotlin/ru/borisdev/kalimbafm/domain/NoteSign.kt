package ru.borisdev.kalimbafm.domain

import com.fasterxml.jackson.annotation.JsonValue

enum class NoteSign(
    val noteName: String,
    val octave: Int,
    val frequency_hz: Double,
    val midi: Int
) {
    H_7("H", 7, 3951.07, 107),
    A_SHARP_7("A#", 7, 3729.31, 106),
    A_7("A", 7, 3520.0, 105),
    G_SHARP_7("G#", 7, 3322.44, 104),
    G_7("G", 7, 3135.96, 103),
    F_SHARP_7("F#", 7, 2959.96, 102),
    F_7("F", 7, 2793.83, 101),
    E_7("E", 7, 2637.02, 100),
    D_SHARP_7("D#", 7, 2489.02, 99),
    D_7("D", 7, 2349.32, 98),
    C_SHARP_7("C#", 7, 2217.46, 97),
    C_7("C", 7, 2093.0, 96),
    H_6("H", 6, 1975.53, 95),
    A_SHARP_6("A#", 6, 1864.66, 94),
    A_6("A", 6, 1760.0, 93),
    G_SHARP_6("G#", 6, 1661.22, 92),
    G_6("G", 6, 1567.98, 91),
    F_SHARP_6("F#", 6, 1479.98, 90),
    F_6("F", 6, 1396.91, 89),
    E_6("E", 6, 1318.51, 88),
    D_SHARP_6("D#", 6, 1244.51, 87),
    D_6("D", 6, 1174.66, 86),
    C_SHARP_6("C#", 6, 1108.73, 85),
    C_6("C", 6, 1046.5, 84),
    H_5("H", 5, 987.767, 83),
    A_SHARP_5("A#", 5, 932.328, 82),
    A_5("A", 5, 880.0, 81),
    G_SHARP_5("G#", 5, 830.609, 80),
    G_5("G", 5, 783.991, 79),
    F_SHARP_5("F#", 5, 739.989, 78),
    F_5("F", 5, 698.456, 77),
    E_5("E", 5, 659.255, 76),
    D_SHARP_5("D#", 5, 622.254, 75),
    D_5("D", 5, 587.33, 74),
    C_SHARP_5("C#", 5, 554.365, 73),
    C_5("C", 5, 523.251, 72),
    H_4("H", 4, 493.883, 71),
    A_SHARP_4("A#", 4, 466.164, 70),
    A_4("A", 4, 440.0, 69),
    G_SHARP_4("G#", 4, 415.305, 68),
    G_4("G", 4, 391.995, 67),
    F_SHARP_4("F#", 4, 369.994, 66),
    F_4("F", 4, 349.228, 65),
    E_4("E", 4, 329.628, 64),
    D_SHARP_4("D#", 4, 311.127, 63),
    D_4("D", 4, 293.665, 62),
    C_SHARP_4("C#", 4, 277.183, 61),
    C_4("C", 4, 261.626, 60),
    H_3("H", 3, 246.942, 59),
    A_SHARP_3("A#", 3, 233.082, 58),
    A_3("A", 3, 220.0, 57),
    G_SHARP_3("G#", 3, 207.652, 56),
    G_3("G", 3, 195.998, 55),
    F_SHARP_3("F#", 3, 184.997, 54),
    F_3("F", 3, 174.614, 53),
    E_3("E", 3, 164.814, 52),
    D_SHARP_3("D#", 3, 155.563, 51),
    D_3("D", 3, 146.832, 50),
    C_SHARP_3("C#", 3, 138.591, 49),
    C_3("C", 3, 130.813, 48),
    H_2("H", 2, 123.471, 47),
    A_SHARP_2("A#", 2, 116.541, 46),
    A_2("A", 2, 110.0, 45),
    G_SHARP_2("G#", 2, 103.826, 44),
    G_2("G", 2, 97.9989, 43),
    F_SHARP_2("F#", 2, 92.4986, 42),
    F_2("F", 2, 87.3071, 41),
    E_2("E", 2, 82.4069, 40),
    D_SHARP_2("D#", 2, 77.7817, 39),
    D_2("D", 2, 73.4162, 38),
    C_SHARP_2("C#", 2, 69.2957, 37),
    C_2("C", 2, 65.4064, 36),
    H_1("H", 1, 61.7354, 35),
    A_SHARP_1("A#", 1, 58.2705, 34),
    A_1("A", 1, 55.0, 33),
    G_SHARP_1("G#", 1, 51.9131, 32),
    G_1("G", 1, 48.9994, 31),
    F_SHARP_1("F#", 1, 46.2493, 30),
    F_1("F", 1, 43.6535, 29),
    E_1("E", 1, 41.2034, 28),
    D_SHARP_1("D#", 1, 38.8909, 27),
    D_1("D", 1, 36.7081, 26),
    C_SHARP_1("C#", 1, 34.6478, 25),
    C_1("C", 1, 32.7032, 24),
    H_0("H", 0, 30.8677, 23),
    A_SHARP_0("A#", 0, 29.1352, 22),
    A_0("A", 0, 27.5, 21),
    UNDEFINED("", 0, 0.0, 0);

    @JsonValue
    fun fullName(): String {
        return noteName + octave;
    }

    companion object {
        fun fromFullName(noteStr: String): NoteSign {
            return values().find { n -> n.fullName() == noteStr } ?: UNDEFINED
        }
    }

    fun fromMidiNumber(midi: Int): NoteSign {
        return values().find { e -> e.midi == midi } ?: UNDEFINED
    }

    override fun toString(): String {
        return fullName()
    }

    fun transpose(tonalityDiff: Int): NoteSign {
        return fromMidiNumber(midi + tonalityDiff)
    }


}