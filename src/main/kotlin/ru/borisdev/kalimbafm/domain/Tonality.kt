package ru.borisdev.kalimbafm.domain

import com.fasterxml.jackson.annotation.JsonValue
import com.wrapper.spotify.enums.Modality

enum class Tonality(
    val tonalityName: String
) {
    C("C"),
    C_SHARP("C#"),
    D("D"),
    D_SHARP("D#"),
    E("E"),
    F("F"),
    F_SHARP("F#"),
    G("G"),
    G_SHARP("G#"),
    A("A"),
    A_SHARP("A#"),
    H("H"),
    Cm("Cm"),
    C_SHARP_m("C#m"),
    Dm("Dm"),
    D_SHARP_m("D#m"),
    Em("Em"),
    Fm("Fm"),
    F_SHARP_m("F#m"),
    Gm("Gm"),
    G_SHARP_m("G#m"),
    Am("Am"),
    A_SHARP_m("A#m"),
    Hm("Hm"),
    NONE("null");

    @JsonValue
    fun displayName(): String {
        return tonalityName;
    }

    override fun toString(): String {
        return displayName()
    }

    fun transpose(tonalityDiff: Int): Tonality {
        return fromTitle(
            NoteSign.fromFullName(noteName() + 4)
                .transpose(tonalityDiff)
                .noteName + modality()
        )
    }

    private fun modality(): String {
        return if (tonalityName.endsWith("m"))
            "m" else ""
    }

    private fun noteName(): String {
        return tonalityName.removeSuffix("m")
    }

    companion object {
        fun parse(
            key: String,
            mode: Modality
        ): Tonality {
            return Am
        }

        fun fromTitle(tonalityName: String): Tonality {
            return values().find { e -> e.tonalityName == tonalityName } ?: NONE
        }
    }


}