package ru.borisdev.kalimbafm.domain

data class VbTrack(
    val id: Long,
    val name: String,
    val artist: String,
    val spotifyLink: String?,
    val tonality: Tonality,
    val notes: List<KNote>,
    val uniqNotes: Set<NoteSign>
) {

}
