package ru.borisdev.kalimbafm.domain

data class TrackDto(
    val id: Long,
    val name: String,
    val artist: String,
    val spotifyLink: String?,
    val tonality: Tonality?,
    val tonalityDiff: Int,
    val differenceInNotes: Int, //Сколько нот не хватает, чтобы полностью сыграть на инструменте
    val uniqNotes: Set<NoteSign>
)
