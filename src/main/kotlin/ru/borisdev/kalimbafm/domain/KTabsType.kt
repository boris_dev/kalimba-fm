package ru.borisdev.kalimbafm.domain

enum class KTabsType {
    LETTER, NUMBER, SIMPLE_NUMBER, SIMPLE_TABS
}
