package ru.borisdev.kalimbafm.domain;


import java.util.Arrays;

/**
 * Created by bshestakov on 04.12.2017.
 */

public enum Tonika {
    C("C"),
    C_SHARP("C#"),
    D("D"),
    D_SHARP("D#"),
    E("E"),
    F("F"),
    F_SHARP("F#"),
    G("G"),
    G_SHARP("G#"),
    A("A"),
    A_SHARP("A#"),
    H("H"),
    NONE("N"),
    ;

    private String name;

    Tonika(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Tonika noteWithDifference(Integer toneDifferenceFromOriginTone) {
        toneDifferenceFromOriginTone = toneDifferenceFromOriginTone % 12;
        NoteSign noteSign = NoteSign.fromTonika(this, 4).plus(toneDifferenceFromOriginTone);
        return noteSign.tonika();
    }


    public static Tonika fromStartString(String s) {
        if (s.contains("#")) {
            String start = s.substring(0, 2);
            return fromString(start);
        } else {
            String start = s.substring(0, 1);
            return fromString(start);
        }
    }

    private static Tonika fromString(String str) {
        for (Tonika tonika : values()) {
            if (str.equals(tonika.name)) {
                return tonika;
            }
        }
        return NONE;
    }

    public static Tonika fromNoteSign(NoteSign noteSign) {
        return Arrays.stream(values())
                .filter(tonika -> tonika.getName().equals(noteSign.getNoteName()))
                .findFirst()
                .get();
    }

    public NoteSign noteSignWithOctave(int octave) {
        return NoteSign.fromTonika(this, octave);
    }
}
