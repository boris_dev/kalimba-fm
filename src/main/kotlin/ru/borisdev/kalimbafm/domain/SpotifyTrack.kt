package ru.borisdev.kalimbafm.domain

import com.wrapper.spotify.enums.Modality

data class SpotifyTrack(val name: String, val key: String, val mode: Modality, val tempo: Float) {

    fun tonality(): Tonality {
        return Tonality.parse(key, mode)
    }

}