package ru.borisdev.kalimbafm.domain


data class KNote(val note: NoteSign, val startTick: Long, val durationTicks: Long){

    override fun toString(): String {
        return "$note($startTick,$durationTicks)"
    }
}

